# **Pass By Value And Pass By Reference In JavaScript**

<p align = "center">
<img src = "https://www.tutsmake.com/wp-content/uploads/2020/05/JavaScript-Pass-by-Value-and-Pass-by-Reference.jpeg">
</p>
<p align = "center">
Fig.1 - pass by value and pass by reference
</p>

To understand the concept better, lets start with simple examples and understand how things are working in the background.

```javascript
let number1 = 10;
let number2 = number1;
number2 = 20;

console.log(number1); //10
console.log(number2); //20
```

```javascript
let personObject1 = {
  name: "Elon Musk",
  company: "Tesla",
};

let personObject2 = personObject1;

personObject2["company"] = "SpaceX";

console.log(personObject1); //{ name: 'Elon Musk', company: 'SpaceX' }
console.log(personObject2); //{ name: 'Elon Musk', company: 'SpaceX' }
```

```javascript
let numbersArray = [2, 4, 7, 8, 10];
let evenNumbers = numbersArray;

evenNumbers[2] = 6;

console.log(numbersArray); //[ 2, 4, 6, 8, 10 ]
console.log(evenNumbers); //[ 2, 4, 6, 8, 10 ]
```

Hold on! What just happened? We are performing the same operation with number, object and array, Why is that objects and arrays original data is getting modified when we change value in cloned object and array, and Why it is not happening with numbers in first example.

To understand this, we need to know about the data types supported by JavaScript. There are two groups,

### 1. **Primitive Type**

- Numbers
- Boolean
- Null
- Undefined

The reason why numbers, boolean, null, undefined comes in _Primitive Type_ is that all these data types have one thing in common, that is the size allocation. All these types have predefined size.

> Number occupies eight bytes of memory and the boolean occupies 1 bit of memory.
> [source](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)

### 2. **Reference Type**

- Arrays
- Objects
- Functions

_Reference Type_ on the other hand includes arrays, objects, and functions. All these types have one thing in common, they are _user-defined_. The size of these types depend on the amount of data it holds, an array can contain thousands of data, an object may contain thousands of key-value pairs and a function may contain thousands of lines of code, all depend on the user.

Cool, now we know the data types, why they are considered as _primitive_ or _reference_. Lets understand why it is working differently when the variable holds Primitive and Reference type.

Whenever the variable holds the primitive type data,
it is actually copying the value and storing in another location referring to new variable.

```javascript
let number1 = 10;
let number2 = number1;
number2 = 20;

console.log(number1); //10
console.log(number2); //20
```

In the example above, the number1 variable is holding primitive type value a Number and when we say number2 is equal to number1, it is actually copying the value, the number1 is referring to and storing it in number2. So, number1 and number2 behaves independently, modifying the value of one variable will not effect the other.

Whereas incase of arrays, objects, the variable will not hold the value directly, since it dont know the size. _The variable hold the memory location address of the array or object where it is stored actually_.

```javascript
let numbersArray = [2, 4, 7, 8, 10];
let evenNumbers = numbersArray;

evenNumbers[2] = 6;

console.log(numbersArray); //[ 2, 4, 6, 8, 10 ]
console.log(evenNumbers); //[ 2, 4, 6, 8, 10 ]
```

Coming back to the example we stated above, numbersArray variable is not storing the value, instead it holds the memory address of where it is stored. So, when we say evenNumbers is equal to numbersArray, they both are pointing to the same memory location where our array is stored. Whenever we modify the value of either variables, both will get impacted. Same with objects and Functions.

Lets see few more examples:

```javascript
let data = [
  {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  },
  {
    id: 2,
    first_name: "Kenneth",
    last_name: "Hinemoor",
    email: "khinemoor1@yellowbook.com",
    gender: "Polygender",
    ip_address: "50.231.58.150",
  },
  {
    id: 3,
    first_name: "Roman",
    last_name: "Sedcole",
    email: "rsedcole2@addtoany.com",
    gender: "Genderqueer",
    ip_address: "236.52.184.83",
  },
  {
    id: 4,
    first_name: "Lind",
    last_name: "Ladyman",
    email: "lladyman3@wordpress.org",
    gender: "Male",
    ip_address: "118.12.213.144",
  },
  {
    id: 5,
    first_name: "Jocelyne",
    last_name: "Casse",
    email: "jcasse4@ehow.com",
    gender: "Agender",
    ip_address: "176.202.254.113",
  },
];

let IpComponents = data.map((object) => {
  object["ip_address"] = object["ip_address"].split(".");

  return object;
});

console.log(data);
console.log(IpComponents);
//Updating the value inside a function also updates the original array.

or;

let IpComponents1 = data.map((object) => {
  let ipAddress = object["ip_address"].split(".");
  object["ip_address"] = ipAddress;

  return object;
});

console.log(IpComponents1);
console.log(data);
//Updating the value inside a function also updates the original array.
```

**Code Explaination**

The data holds array of objects. The map takes the callback function, we are passing object parameter and what ever changes for the variable _object_ we are making it is also reflecting in the original array.Because my _object_ parameter is not holding the value, it is holding the reference. Also in the code we are assigning _ipAddress_ variable with the object, even here it is taking the reference of the object.

## **What if we do not want original Object to be Affected?**

Say, you do not want original object to be affected while working on the cloned array, we can achieve that using _Shallow Copy_ of the original array using _Spread Operator_.

```javascript
let numbers = [1, 2, 3, 4, 5];
let evenNum = [...numbers];

evenNum[0] = 10;

console.log(evenNum); // [10, 2, 3, 4, 5 ]
console.log(numbers); //[ 1, 2, 3, 4, 5 ]
```

In the above code, when we do shallow copying, we are actually copying the values not the references so it behaves like we are working on primitive data type without affecting the original array.

## **Corner Cases While Using Spread Operator**

Lets see few Examples:

```javascript
let data = [
  {
    id: 1,
    color: "Mauv",
    material: "Plastic",
    country_code: "PH",
    amount: 51481,
  },
  {
    id: 2,
    color: "Crimson",
    material: "Granite",
    country_code: "FR",
    amount: 78691,
  },
  {
    id: 3,
    color: "Blue",
    material: "Plexiglass",
    country_code: "ID",
    amount: 16651,
  },
  {
    id: 4,
    color: "Goldenrod",
    material: "Stone",
    country_code: "PL",
    amount: 87242,
  },
  {
    id: 5,
    color: "Teal",
    material: "Plastic",
    country_code: "LU",
    amount: 36197,
  },
];

//Assign the $ by converting amount to strings.

let convertedData = [...data];

let requiredData = convertedData.map((object) => {
  object["amount"] = "$" + object["amount"];

  return object;
});

console.log(requiredData);
console.log(data);
//Both logs same output
[
  {
    id: 1,
    color: 'Mauv',
    material: 'Plastic',
    country_code: 'PH',
    amount: '$51481'
  },
  {
    id: 2,
    color: 'Crimson',
    material: 'Granite',
    country_code: 'FR',
    amount: '$78691'
  },
  {
    id: 3,
    color: 'Blue',
    material: 'Plexiglass',
    country_code: 'ID',
    amount: '$16651'
  },
  {
    id: 4,
    color: 'Goldenrod',
    material: 'Stone',
    country_code: 'PL',
    amount: '$87242'
  },
  {
    id: 5,
    color: 'Teal',
    material: 'Plastic',
    country_code: 'LU',
    amount: '$36197'
  }
]
```
This is because, the array is holding an Object which is a *Reference Type*. So, in map function the object parameter still holds the reference to the address of memory location.

```javascript
let data = [[1,2,3], 4,[5,6]];

let newData = [...data];

newData[0] = 1;

console.log(newData); //[ 1, 4, [ 5, 6 ] ]
console.log(data);//[ [ 1, 2, 3 ], 4, [ 5, 6 ] ]

//If you want to change the nested value
let data = [[1,2,3], 4,[5,6]];

let newData = [...data];

newData[0][1] = 10;

console.log(newData); //[ [ 1, 10, 3 ], 4, [ 5, 6 ] ]
console.log(data); //[ [ 1, 10, 3 ], 4, [ 5, 6 ] ]
```

The other corner case is, when we try to change the values of nested elements, it holds the reference, so even while shallow copying the data, effects the original data.

## **How to OverCome This Affect?**

To completely prevent the original data mutation, one of the methods is *Deep Copying.*

Lets see with an Example:

```javascript
let data = [[1,2,3], 4,[5,6]];

let newData = JSON.parse(JSON.stringify(data))

newData[0][1] = 10;

console.log(newData); //[ [ 1, 10, 3 ], 4, [ 5, 6 ] ]
console.log(data); //[ [ 1, 2, 3 ], 4, [ 5, 6 ] ]
```

When you stringify the data, it transforms the original data and it no longer a reference type, so when we perform changes to the data if will not affect the original data.

## **Key TakeAway**
> *When the variable holds primitive data value, making changes to cloned data will not have affect on original data. However if the variable holds the data which is of reference data type, making changes to the cloned data will affect the original data*.


## **References**
* [GeeksForGeeks](https://www.geeksforgeeks.org/pass-by-value-and-pass-by-reference-in-javascript/)
* [Mozilla Developer Document](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)
* [Flexiple](https://flexiple.com/javascript-pass-by-reference-or-value)
* [O'Reilly](https://www.oreilly.com/library/view/javascript-the-definitive/0596000480/ch04s04.html)
* [Fig.1-Image-Credits](https://www.tutsmake.com/wp-content/uploads/2020/05/JavaScript-Pass-by-Value-and-Pass-by-Reference.jpeg)