let number1 = 10;
let number2 = number1
number2 = 20;

// console.log(number1)
// console.log(number2);

let personObject1 = {
    name : "Elon Musk",
    company : "Tesla"
};

let personObject2 = personObject1;

personObject2['company'] = "SpaceX";

// console.log(personObject1);
// console.log(personObject2);

let numbersArray = [2, 4, 7, 8, 10];
let evenNumbers = numbersArray;

evenNumbers[2] = 6;

// console.log(numbersArray); //[ 2, 4, 6, 8, 10 ]
// console.log(evenNumbers); //[ 2, 4, 6, 8, 10 ]

// let data = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
// {"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
// {"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
// {"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
// {"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
// {"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
// {"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
// {"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
// {"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
// {"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}];

//split the ip addresses into components.

// let IpComponents = data.map((object) => {
//     object['ip_address'] = object['ip_address'].split(".");

//     return object;
// })

// console.log(data);
// console.log(IpComponents);

// let IpComponents1 = data.map((object) => {
//     let ipAddress = object['ip_address'].split(".");
//     let myAddress = ipAddress[0];
//     object['ip_address'] = myAddress;

//     return object;
// })

// console.log(IpComponents1)
// console.log(data)

// let numbers = [1,2,3,4,5];
// let evenNum = [...numbers];

// evenNum[0] = 10;

// console.log(evenNum);
// console.log(numbers);

// let data = [{"id":1,"color":"Mauv","material":"Plastic","country_code":"PH","amount":51481},
// {"id":2,"color":"Crimson","material":"Granite","country_code":"FR","amount":78691},
// {"id":3,"color":"Blue","material":"Plexiglass","country_code":"ID","amount":16651},
// {"id":4,"color":"Goldenrod","material":"Stone","country_code":"PL","amount":87242},
// {"id":5,"color":"Teal","material":"Plastic","country_code":"LU","amount":36197}];

// //Assign the $ by converting amount to strings.

// let convertedData = [...data];

// let requiredData = convertedData.map((object) => {
//     object['amount'] = "$" + object['amount'];

//     return object;
// })

// console.log(requiredData);
// console.log(data);

let data = [[1,2,3], 4,[5,6]];

let newData = JSON.parse(JSON.stringify(data))

newData[0][1] = 10;

console.log(newData);
console.log(data);
